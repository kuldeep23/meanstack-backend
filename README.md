
# Mean Project Backend

This project is for Product Details 

## Clone this repository

```
git clone repositry url
```

Then install the dependencies

npm install

## Start the server

Run in development mode
```
npm start

The default URL is: *http://localhost:8080*