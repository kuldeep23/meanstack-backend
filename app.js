const express = require("express")
const mongoose = require("mongoose")
const dotenv = require("dotenv")
const cors = require('cors');
const path=require('path')
const bodyParser = require("body-parser");
const auth = require("./middleware/auth");

const app = express();
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
// parse requests of content-type - application/json
app.use(bodyParser.json());


//configurations
dotenv.config();// for env variables
app.use(cors());// CORS

//Include Routes
require("./routes/ProductRoute")(app)
require("./routes/UserRoute")(app)

app.use(express.static(path.join(__dirname,'public')));


// // //All API
// app.all('*', auth.verifyToken,function(req, res, next) {
//   next()
// });

app.get('*',(req,res)=>{
res.sendFile(path.join(__dirname,'public/index.html'))
})

mongoose.set('strictQuery', false);
mongoose
  .connect(process.env.MONGO_URL,
    { useNewUrlParser: true, useUnifiedTopology: true, autoIndex: true }
  )
  .then((response) => {
    console.log('Connected to MongoDB Successfully')
  })
  .catch((error) => {
    console.error(error);
  });

app.listen({ port: process.env.PORT }, () =>
  console.log(`Express Server is Ready at port :${process.env.PORT}`)
);