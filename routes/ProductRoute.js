module.exports = app => {
    const productController = require("../controllers/ProductController.js");
    const validator=require("../validators/ProductValidator")
    const auth = require("../middleware/auth");
    const router = require("express").Router();
    
    //Add Product and get  All Product 
    router.post("/products",auth.verifyToken,productController.addProduct);
    router.get("/products",auth.verifyToken,productController.getAllProducts);

    //Update Product ,Get Single Product & Delete Product
    router.put("/products/:productId",auth.verifyToken,productController.updateProduct);
    router.get("/products/:productId",auth.verifyToken,productController.getProduct)
    router.delete("/products/:productId",auth.verifyToken,productController.deleteProduct)

   
    app.use('/api/v1', router);
  };