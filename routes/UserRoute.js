module.exports = app => {
    const userController = require("../controllers/UserController.js");
    const validator=require("../validators/UserValidator")
    const router = require("express").Router();

    //Register User and Login User
    router.post("/register",userController.addUser);
    router.post("/login",userController.loginUser);

    app.use('/api/v1', router);
};