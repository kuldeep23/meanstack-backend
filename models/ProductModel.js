const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ProductSchema = new Schema({

    name: {
        type: String,
        required: true,
    },
    price: {
        type: String,
        required: true,
    },
    quantity: {
        type: String,
        required: true,
    },
    info: {
        type: String,
        required: true,
    },
},

    { timestamps: true }
);

module.exports = mongoose.model("products", ProductSchema);