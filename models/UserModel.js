const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const UserSchema = new Schema({
    firstName: {
        type: String,
        default:null,
    },
    lastName: {
        type: String,
        default:null,
    },
    email: {
        type: String,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    token: { 
        type: String 
    },
},

    { timestamps: true }
);

module.exports = mongoose.model("user", UserSchema);
