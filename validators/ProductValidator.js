const Joi = require("joi");

exports.productValidator = async (req, res, next) => {

    try {
      //  console.log('# ProductValidator -> checkProduct  -> start ');
        const productSchema = Joi.object({
            name: Joi.string().min(3).max(20).required(),
            price: Joi.string().required(),
            quantity: Joi.string().required(),
            info: Joi.string().min(5).max(100).required()
        })
        const validation = await productSchema.validateAsync(req.body);
        if (validation.error) {
            return res.error('INVALID_DATA');
        }
        return next();
    } catch (error) {
       //console.log("Product Validation Error:", error)
        res.send({ err: error })
    }
}