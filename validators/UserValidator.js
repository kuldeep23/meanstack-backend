const Joi = require("joi");

exports.userValidator = async (req, res, next) => {

    try {
      //  console.log('# UserValidator -> checkUserInput  -> start ');
        const userSchema = Joi.object({
            firstName: Joi.string().required(),
            lastName: Joi.string().required(),
            email: Joi.string().email().required(),
            password: Joi.string().min(5).max(50).required()
        })
        const validation = await userSchema.validateAsync(req.body);
        if (validation.error) {
            return res.error('INVALID_DATA');
        }
        return next();
    } catch (error) {
       //console.log("User Validation Error:", error)
        res.send({ err: error })
    }
}