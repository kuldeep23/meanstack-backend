const jwt = require("jsonwebtoken");

exports.generateToken = (userId, email) => {
    try {
        
        const token = jwt.sign(
            { user_id: userId, email },
            process.env.TOKEN_KEY,
            {
                expiresIn: "2h",
            }
        );
        
        return token;

    } catch (error) {
        return res.status(401)
    }

}

exports.verifyToken = (req, res, next) => {
    try {
        const token =
            req.headers.authorization.split(' ')[1];

        if (!token) {
            return res.status(403).send("A token is required for authentication");
        }

        const decoded = jwt.verify(token, process.env.TOKEN_KEY);
        req.user = decoded;

        return next();
    } catch (err) {
        return res.status(401).send("Invalid Token");
    }

};
