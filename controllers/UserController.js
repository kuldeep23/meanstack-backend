const jwt = require("jsonwebtoken");
const User = require("../models/UserModel");
const { generateToken } = require("../middleware/auth")
const bcrypt = require("bcryptjs")

//Register new User 
exports.addUser = async (req, res) => {

    try {
        // Get user input
        const { firstName, lastName, email, password } = req.body;

        // Validate user input
        if (!(email && password && firstName && lastName)) {
            res.status(400).send("All input is required");
        }

        // check if user already exist
        const oldUser = await User.findOne({ email });

        if (oldUser) {
            return res.status(409).send("User Already Exist. Please Login");
        }

        //Encrypt user password
        encryptedPassword = await bcrypt.hash(password, 10);

        // Create user in our database
        const user = await User.create({
            firstName,
            lastName,
            email: email.toLowerCase(), // sanitize: convert email to lowercase
            password: encryptedPassword,
        });

        // return new user
        res.status(201).json(user);
    } catch (error) {
        // console.log(error);
        res.status(500).send({ err: error })
    }
}


//Login
exports.loginUser = async (req, res) => {

    try {
        // Get user input
        const { email, password } = req.body;

        // Validate user input
        if (!(email && password)) {
            res.status(400).send("All input is required");
        }
        // Validate if user exist in our database
        const user = await User.findOne({ email });
        console.log(user)

        if (user && (await bcrypt.compare(password, user.password))) {
            console.log("///////////////")
            // Create token
            const token = await generateToken(user._id, email)
            console.log(token)

            // save user token
            user.token = token;

            res.status(200).json({ "token": token });
        }
        // else {
            res.status(400).send("Invalid Credentials");
        // }
         
    } catch (error) {
        console.log(error);
        res.status(500).send({ err: error })
    }
}