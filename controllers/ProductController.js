const Product = require("../models/ProductModel")

//Add new Product 
exports.addProduct = async (req, res) => {
    try {
        let product = {
            name: req.body.name,
            price: req.body.price,
            quantity: req.body.quantity,
            info: req.body.info
        }
        //check if the product already exists
        let existingProduct = await Product.findOne({ name: product.name })
        if (existingProduct) {
            return res.status(401).send({ msg: "Product is already exits" })

        }

        //create the product
        let newProduct = new Product(product)
        product = await newProduct.save();
        res.status(200).send(product)

    } catch (error) {
      //console.log(error);
        res.status(500).send({ err: error })
    }
}

//update Product
exports.updateProduct = async (req, res) => {
    let  {productId}  = req.params;
    try {
        let updatedProduct = {
            name: req.body.name,
            price: req.body.price,
            quantity: req.body.quantity,
            info: req.body.info
        }

        //check if the product is exists
        let product = await Product.findById(productId);
        if (!product) {
            return res.status(404).send({ msg: "Product is not Exists!" })
        }

        //update Product
        updateProduct = await Product.findByIdAndUpdate(productId, {
            $set: {
                name: updatedProduct.name ? updatedProduct.name : product.name,
                price: updatedProduct.price ? updatedProduct.price : product.price,
                quantity: updatedProduct.quantity ? updatedProduct.quantity : product.quantity,
                info: updatedProduct.info ? updatedProduct.info : product.info

            }
        })
        res.status(200).send(updateProduct)
    } catch (error) {
        //console.log(error);
        res.status(500).send({ err: error })
    }
}

//get Product By Id
exports.getProduct = async (req, res) => {
    let { productId } = req.params;
    try {
        let product = await Product.findById(productId)
        if (!product) {
            return res.status(404).send({ msg: "Product is Not Found!" })

        }
        res.status(200).send(product)
    } catch (error) {
       // console.log(error);
        res.status(500).send({ err: error })
    }
}

//get All Product
exports.getAllProducts = async (req, res) => {
    try {

        let product = await Product.find();
        res.status(200).send(product)

    } catch (error) {
        //console.log(error);
        res.status(500).send({ err: error })
    }
}

//delete Product by Id
exports.deleteProduct = async (req, res) => {
    let { productId } = req.params;
    try {
        let product = await Product.findById(productId)
        if (!product) {
            return res.status(404).send({ msg: "Product is Not Found!" })
        }

        //delete product
        product = await Product.findByIdAndDelete(productId)
        res.status(200).send({ msg: "Product is deleted Successfully" })

    } catch (error) {
      //  console.log(error);
        res.status(500).send({ err: error })
    }
}